# Práctica Final: JQuery, Ajax, DOM, JSON
>Muciño Entzana Guillermo Josue
------------------
<ol>

<li>Reutilice el codigo del ejemplo index.js </li>
<li>Modifique los ids que corresponden a "miLista" que hacen referencia al archivo buscar.html  </li>
<li>La url se modifico a "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra, donde palabra es la busqueda que estamos haciendo 
</li>
<li>Dentro de ajax pregunte si (respuesta.results.length) era 0 de ser asi no encontro un resultado y se oculta el gif de cargar </li>
<li>Agregue (movie.original_title) y  (movie.overview) en la funcion crearMovieCard algunos atributos como el genero solo aparecian como un id de un catalogo y decidi quitarlos </li>
</ol>